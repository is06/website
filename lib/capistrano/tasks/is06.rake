namespace :is06 do
    task :prebuild do
        run_locally do
            within fetch(:rsync_local_cache) do
                execute :composer, 'install --no-dev --optimize-autoloader'
                execute :yarn
                execute :yarn, "encore production"
            end
        end
    end

    after 'rsync:update_local_cache', 'is06:prebuild'
end
