<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class PageController
 * @package App\Controller
 */
class PageController extends AbstractController
{
    /**
     * @Route(
     *     name="home",
     *     path="/",
     *     methods={"GET"}
     * )
     */
    public function home()
    {
        return $this->render('pages/home.html.twig');
    }

    /**
     * @Route(
     *     name="games",
     *     path="/games",
     *     methods={"GET"}
     * )
     */
    public function games()
    {
        return $this->render('pages/games.html.twig');
    }

    /**
     * @Route(
     *     name="game",
     *     path="/games/{name}",
     *     methods={"GET"}
     * )
     */
    public function game($name)
    {
        if ($name !== 'squibreeze') {
            throw new NotFoundHttpException();
        }

        return $this->render('pages/games/'.$name.'.html.twig');
    }

    /**
     * @Route(
     *     name="team",
     *     path="/team",
     *     methods={"GET"}
     * )
     */
    public function team()
    {
        return $this->render('pages/team.html.twig');
    }
}
